# Custom App plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](http://getcomposer.org).

The recommended way to install composer packages is:

```
composer require it-mk/cakephp-custom-app
```


## Bake
If you need to bake customised controller actions and templates, copy the following into your app's bootstrap_cli.php and customise as needed:

```
Plugin::load('MK/CustomApp', ['bootstrap' => true, 'routes' => true]);
// Customise controller actions / templates
EventManager::instance()->on(
    'Bake.beforeRender.Controller.controller',
    function (Event $event) {
        $view = $event->getSubject();
        // Set the actions for the controller
        $view->set('actions', [
            'index',
            'view',
            'add',
            'edit',
            'delete',
            'new_method'
        ]);
    }
);
```
Then run the 'bake' command using the 'theme' argument. For example:
```
bin/cake bake model customer_order_items --theme=MK/CustomApp
```
