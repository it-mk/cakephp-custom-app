<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace MK\CustomApp\Shell\Task;

use Cake\Console\Shell;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Core\Plugin;
use Cake\Filesystem\Folder;
use Cake\Http\Response;
use Cake\Http\ServerRequest as Request;
use Cake\ORM\Association;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use ReflectionClass;
use Bake\Shell\Task\TestTask as CakeTestTask;

/**
 * Task class for creating and updating test files.
 *
 * @property \Bake\Shell\Task\BakeTemplateTask $BakeTemplate
 */
class TestTask extends CakeTestTask
{
    
    /**
     * Completes final steps for generating data to create test case.
     *
     * @param string $type Type of object to bake test case for ie. Model, Controller
     * @param string $className the 'cake name' for the class ie. Posts for the PostsController
     * @return string|false
     */
    public function bake($type, $className)
    {
        $type = $this->normalize($type);
        if (!isset($this->classSuffixes[$type]) || !isset($this->classTypes[$type])) {
            return false;
        }

        $fullClassName = $this->getRealClassName($type, $className);

        if (empty($this->params['no-fixture'])) {
            if (!empty($this->params['fixtures'])) {
                $fixtures = array_map('trim', explode(',', $this->params['fixtures']));
                $this->_fixtures = array_filter($fixtures);
            } elseif ($this->typeCanDetectFixtures($type) && class_exists($fullClassName)) {
                $this->out('Bake is detecting possible fixtures...');
                $testSubject = $this->buildTestSubject($type, $fullClassName);
                $this->generateFixtureList($testSubject);
            }
        }
        // MK Mod: Use factories instead
        if($type === 'Controller') {
            $factory = Inflector::singularize($className) .'Factory';
            $this->BakeTemplate->set('factory',$factory);
        }
        // End mod
        
        $methods = [];
        if (class_exists($fullClassName)) {
            $methods = $this->getTestableMethods($fullClassName);
        }
        // MK Mod: Unset manage method
        foreach($methods as $k => $m) {
            if($m === 'manage') {
                unset($methods[$k]);
            }
        }
        // End MK Mod
        $mock = $this->hasMockClass($type);
        list($preConstruct, $construction, $postConstruct) = $this->generateConstructor($type, $fullClassName);
        $uses = $this->generateUses($type, $fullClassName);

        $subject = $className;
        list($namespace, $className) = namespaceSplit($fullClassName);

        $baseNamespace = Configure::read('App.namespace');
        if ($this->plugin) {
            $baseNamespace = $this->_pluginNamespace($this->plugin);
        }
        $subNamespace = substr($namespace, strlen($baseNamespace) + 1);

        $properties = $this->generateProperties($type, $subject, $fullClassName);

        $this->out("\n" . sprintf('Baking test case for %s ...', $fullClassName), 1, Shell::QUIET);

        $this->BakeTemplate->set('fixtures', $this->_fixtures);
        $this->BakeTemplate->set('plugin', $this->plugin);
        $this->BakeTemplate->set(compact(
            'subject',
            'className',
            'properties',
            'methods',
            'type',
            'fullClassName',
            'mock',
            'type',
            'preConstruct',
            'postConstruct',
            'construction',
            'uses',
            'baseNamespace',
            'subNamespace',
            'namespace'
        ));
        $out = $this->BakeTemplate->generate('tests/test_case');

        $filename = $this->testCaseFileName($type, $fullClassName);
        $emptyFile = $this->getPath() . $this->getSubspacePath($type) . DS . 'empty';
        $this->_deleteEmptyFile($emptyFile);
        if ($this->createFile($filename, $out)) {
            return $out;
        }

        return false;
    }
    
    /**
     * Checks whether the chosen type can find its own fixtures.
     * Currently only model, and controller are supported
     *
     * @param string $type The Type of object you are generating tests for eg. controller
     * @return bool
     */
    public function typeCanDetectFixtures($type)
    {
        return false;
    }
    
    /**
     * Generate property info for the type and class name
     *
     * The generated property info consists of a set of arrays that hold the following keys:
     *
     * - `description` (the property description)
     * - `type` (the property docblock type)
     * - `name` (the property name)
     * - `value` (optional - the properties initial value)
     *
     * @param string $type The Type of object you are generating tests for eg. controller
     * @param string $subject The name of the test subject.
     * @param string $fullClassName The Classname of the class the test is being generated for.
     * @return array An array containing property info
     */
    public function generateProperties($type, $subject, $fullClassName)
    {
        $properties = [];
        switch ($type) {
            case 'Cell':
                $properties[] = [
                    'description' => 'Request mock',
                    'type' => '\Cake\Http\ServerRequest|\PHPUnit_Framework_MockObject_MockObject',
                    'name' => 'request',
                ];
                $properties[] = [
                    'description' => 'Response mock',
                    'type' => '\Cake\Http\Response|\PHPUnit_Framework_MockObject_MockObject',
                    'name' => 'response',
                ];
                break;

            case 'Shell':
            case 'Task':
                $properties[] = [
                    'description' => 'ConsoleIo mock',
                    'type' => '\Cake\Console\ConsoleIo|\PHPUnit_Framework_MockObject_MockObject',
                    'name' => 'io',
                ];
                break;

            case 'ShellHelper':
                $properties[] = [
                    'description' => 'ConsoleOutput stub',
                    'type' => '\Cake\TestSuite\Stub\ConsoleOutput',
                    'name' => 'stub',
                ];
                $properties[] = [
                    'description' => 'ConsoleIo mock',
                    'type' => '\Cake\Console\ConsoleIo',
                    'name' => 'io',
                ];
                break;
        }

        if (!in_array($type, ['Controller', 'Command'])) {
            $properties[] = [
                'description' => 'Test subject',
                'type' => '\\' . $fullClassName,
                'name' => $subject,
            ];
        }
        
        if (in_array($type, ['Controller'])) {
            $properties[] = [
                'description' => 'Base resource route',
                'type' => 'string',
                'name' => 'baseRoute',
                'value' => "'/". Inflector::dasherize(Inflector::tableize($subject)) ."'"
            ];
        }

        return $properties;
    }

}
