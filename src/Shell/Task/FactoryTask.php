<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace MK\CustomApp\Shell\Task;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\Database\Exception;
use Cake\Database\Schema\TableSchema;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\Utility\Text;
use DateTimeInterface;
use Bake\Shell\Task\BakeTask;

/**
 * Task class for creating and updating fixtures files.
 *
 * @property \Bake\Shell\Task\BakeTemplateTask $BakeTemplate
 * @property \Bake\Shell\Task\ModelTask $Model
 */
class FactoryTask extends BakeTask
{
    /**
     * Tasks to be loaded by this Task
     *
     * @var array
     */
    public $tasks = [
        'Bake.Model',
        'Bake.BakeTemplate',
    ];

    /**
     * Get the file path.
     *
     * @return string
     */
    public function getPath()
    {
        $dir = 'Factory/';
        $path = defined('TESTS') ? TESTS . $dir : ROOT . DS . 'tests' . DS . $dir;
        if (isset($this->plugin)) {
            $path = $this->_pluginPath($this->plugin) . 'tests/' . $dir;
        }

        return str_replace('/', DS, $path);
    }

    /**
     * Gets the option parser instance and configures it.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser = $parser->setDescription(
            'Generate model factory for use with the test suite.'
        )->addArgument('name', [
            'help' => 'Name of the factory to bake (without the `Factory` suffix). ' .
                'You can use Plugin.name to bake plugin factories.',
        ])->addOption('table', [
            'help' => 'The table name if it does not follow conventions.',
        ]);

        return $parser;
    }

    /**
     * Execution method always used for tasks
     * Handles dispatching to interactive, named, or all processes.
     *
     * @param string|null $name The name of the fixture to bake.
     * @return null|bool
     */
    public function main($name = null)
    {
        parent::main();
        $name = $this->_getName($name);

        if (empty($name)) {
            $this->out('Choose a factory to bake from the following:');
            foreach ($this->Model->listUnskipped() as $table) {
                $this->out('- ' . $this->_camelize($table));
            }

            return true;
        }

        $table = null;
        if (isset($this->params['table'])) {
            $table = $this->params['table'];
        }
        $model = $this->_camelize($name);
        $this->bake($model, $table);
    }

    /**
     * Assembles and writes a Fixture file
     *
     * @param string $model Name of model to bake.
     * @param string|null $useTable Name of table to use.
     * @return string Baked fixture content
     * @throws \RuntimeException
     */
    public function bake($model, $useTable = null)
    {
        $table = $schema = $records = $import = $modelImport = null;

        if (!$useTable) {
            $useTable = Inflector::tableize($model);
        } elseif ($useTable !== Inflector::tableize($model)) {
            $table = $useTable;
        }

        $importBits = [];
        if (!empty($this->params['schema'])) {
            $modelImport = true;
            $importBits[] = "'table' => '{$useTable}'";
        }
        if (!empty($importBits) && $this->connection !== 'default') {
            $importBits[] = "'connection' => '{$this->connection}'";
        }
        if (!empty($importBits)) {
            $import = sprintf("[%s]", implode(', ', $importBits));
        }

        try {
            $data = $this->readSchema($model, $useTable);
        } catch (Exception $e) {
            TableRegistry::getTableLocator()->remove($model);
            $useTable = Inflector::underscore($model);
            $table = $useTable;
            $data = $this->readSchema($model, $useTable);
        }

        if ($modelImport === null) {
            $schema = $this->_generateSchema($data);
        }
        $schemaColumns = $this->schemaColumns($data);
        $generatorColumns = $this->generatorColumns($data);
        return $this->generateFactoryFile($model, compact('table', 'schema', 'import', 'generatorColumns','schemaColumns'));
    }

    /**
     * Get schema metadata for the current table mapping.
     *
     * @param string $name The model alias to use
     * @param string $table The table name to get schema metadata for.
     * @return \Cake\Database\Schema\TableSchema
     */
    public function readSchema($name, $table)
    {
        $connection = ConnectionManager::get($this->connection);

        if (TableRegistry::getTableLocator()->exists($name)) {
            $model = TableRegistry::getTableLocator()->get($name);
        } else {
            $model = TableRegistry::getTableLocator()->get($name, [
                'table' => $table,
                'connection' => $connection,
            ]);
        }

        return $model->getSchema();
    }

    /**
     * Generate the fixture file, and write to disk
     *
     * @param string $model name of the model being generated
     * @param array $otherVars Contents of the fixture file.
     * @return string Content saved into fixture file.
     */
    public function generateFactoryFile($model, array $otherVars)
    {
        $alias = Inflector::camelize(Inflector::pluralize($model));
        $defaults = [
            'name' => $model,
            'table' => null,
            'schema' => null,
            'records' => null,
            'import' => null,
            'fields' => null,
            'registryName' => ($this->plugin) ? $this->plugin .'.'. $alias : $alias,
            'namespace' => Configure::read('App.namespace'),
            'alias' => $alias,
            'schemaColumns' => null,
            'generatorColumns' => null
        ];
        if ($this->plugin) {
            $defaults['namespace'] = $this->_pluginNamespace($this->plugin);
        }
        
        $vars = $otherVars + $defaults;

        $path = $this->getPath();
        $filename = $vars['name'] . 'Factory.php';

        $this->BakeTemplate->set('model', $model);
        $this->BakeTemplate->set($vars);
        $content = $this->BakeTemplate->generate('tests/factory');

        $this->out("\n" . sprintf('Baking test factory for %s...', $model), 1, Shell::QUIET);
        $this->createFile($path . $filename, $content);
        $emptyFile = $path . 'empty';
        $this->_deleteEmptyFile($emptyFile);

        return $content;
    }
    
    /**
     * Generates a string representation of a schema.
     *
     * @param \Cake\Database\Schema\TableSchema $table Table schema
     * @return string fields definitions
     */
    protected function schemaColumns(TableSchema $table)
    {
        $cols = [];
        foreach ($table->columns() as $field) {
            $cols[$field] = $table->getColumn($field);
        }
        
        return $cols;
    }
    
    /**
     * Generates a string representation of a schema.
     *
     * @param \Cake\Database\Schema\TableSchema $table Table schema
     * @return string fields definitions
     */
    protected function generatorColumns(TableSchema $table)
    {
        $cols = [];
        foreach ($table->columns() as $field) {
            // Skip associations and id,deleted
            if(substr($field,-3,3) === '_id' || substr($field,-3,3) === '_by' || in_array($field,['id','deleted'])) {
                continue;
            }
            $generatorString = 'null';
            $cfg = $table->getColumn($field);
            switch($cfg['type']) {
                case 'boolean':
                    $generatorString = 'rand(0,1)'; 
                    break;
                case 'integer':
                    $generatorString = 'rand(0,127)';
                    break;
                case 'text':
                    $generatorString = '$faker->paragraph()';
                    break;
                case 'string':
                    $generatorString = 'Text::truncate($faker->sentence(),'. $cfg['length'] .')';
                    break;
                case 'float':
                case 'decimal':
                    $generatorString = '$faker->randomFloat('. $cfg['precision'] .',0,10)';
                    break;
                case 'date':
                    $generatorString = '(new Time)->toDateString()';
                    break;
                case 'datetime':
                    $generatorString = '(new Time)->toDateTimeString()';
                    break;
            }
            $cols[$field] = $generatorString;
        }
        
        return $cols;
    }
    
    /**
     * Generates a string representation of a schema.
     *
     * @param \Cake\Database\Schema\TableSchema $table Table schema
     * @return string fields definitions
     */
    protected function _generateSchema(TableSchema $table)
    {
        $cols = $indexes = $constraints = [];
        foreach ($table->columns() as $field) {
            $fieldData = $table->getColumn($field);
            $properties = implode(', ', $this->_values($fieldData));
            $cols[] = "        '$field' => [$properties],";
        }
        foreach ($table->indexes() as $index) {
            $fieldData = $table->getIndex($index);
            $properties = implode(', ', $this->_values($fieldData));
            $indexes[] = "            '$index' => [$properties],";
        }
        foreach ($table->constraints() as $index) {
            $fieldData = $table->getConstraint($index);
            $properties = implode(', ', $this->_values($fieldData));
            $constraints[] = "            '$index' => [$properties],";
        }
        $options = $this->_values($table->getOptions());

        $content = implode("\n", $cols) . "\n";
        if (!empty($indexes)) {
            $content .= "        '_indexes' => [\n" . implode("\n", $indexes) . "\n        ],\n";
        }
        if (!empty($constraints)) {
            $content .= "        '_constraints' => [\n" . implode("\n", $constraints) . "\n        ],\n";
        }
        if (!empty($options)) {
            foreach ($options as &$option) {
                $option = '            ' . $option;
            }
            $content .= "        '_options' => [\n" . implode(",\n", $options) . "\n        ],\n";
        }

        return "[\n$content    ]";
    }

    /**
     * Formats Schema columns from Model Object
     *
     * @param array $values options keys(type, null, default, key, length, extra)
     * @return array Formatted values
     */
    protected function _values($values)
    {
        $vals = [];
        if (!is_array($values)) {
            return $vals;
        }
        foreach ($values as $key => $val) {
            if (is_array($val)) {
                $vals[] = "'{$key}' => [" . implode(", ", $this->_values($val)) . "]";
            } else {
                $val = var_export($val, true);
                if ($val === 'NULL') {
                    $val = 'null';
                }
                if (!is_numeric($key)) {
                    $vals[] = "'{$key}' => {$val}";
                } else {
                    $vals[] = "{$val}";
                }
            }
        }

        return $vals;
    }

}
