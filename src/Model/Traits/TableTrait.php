<?php
namespace MK\CustomApp\Model\Traits;

use Cake\ORM\Query;

trait TableTrait
{

    /*--------------------------------------------------------------------------
     * Finders
     *--------------------------------------------------------------------------
     */
    
    /**
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findWithConstraints(Query $query, $options)
    {
        return $this->addFindConstraints($query,$options);
    }

    /*--------------------------------------------------------------------------
     * Helper methods
     *--------------------------------------------------------------------------
     */
    public function findOrMake($constraints)
    {
        $record = $this->find()->where($constraints)->first();
        if ($record) {
            return $record;
        }
        $entity = $this->newEntity();
        $entity->set($constraints, ['guard' => false]);
        return $entity;
    }
    
    
}