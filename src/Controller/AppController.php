<?php
namespace MK\CustomApp\Controller;

use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;

use CakePdf\Pdf\CakePdf;

class AppController extends Controller
{
    
    protected $is_test = null;

    public function beforeFilter(Event $event) 
    {
        $session = $this->request->getSession();
        $this->is_test = ($session && $session->read('test'));
    }
    
    /**
     * Creates response, given specified type, and options.
     * @param string $type
     * @param array $options
     */
    protected function dynamicResponse($options=[],$errors=null)
    {
        $options += [
            'data' => []
        ];
        if($this->is_test && !empty($errors)) {
            debug($errors);
            $traceLine = \data_get(debug_backtrace(),1);
            //debug($traceLine);
            debug('Trace: '. \data_get($traceLine,'class').'::'.\data_get($traceLine,'function'));
            $itemErrors = (\data_get($options,'data.item')) ? $options['data']['item']->getErrors() : null;
            if(!empty($itemErrors)) {
                debug($itemErrors);
            }
        }
        // JSON response
        if($this->request->is('json')) {
            return $this->jsonResponse($options,$errors);
        } 
        // AJAX - Text response
        elseif($this->request->is(['text','ajax'])) {
            return $this->textResponse($options,$errors);
        }
        // PDF response
        elseif($this->request->is(['pdf'])) {
            return $this->pdfResponse($options,$errors);
        }
        // XLS response
        elseif($this->request->is(['xls','xlsx'])) {
            return $this->xlsResponse($options,$errors);
        } 
        // HTML response
        else {
            return $this->htmlResponse($options,$errors);
        }
    }
    
    /**
     * Creates JSON response for AJAX, given data array.
     * @param array $options
     * @param boolean $assoc - Whether to encode as JSON object
     */
    protected function jsonResponse($options=[],$errors=null)
    {
        $options += [
            'data' => [],
            'assoc' => true
        ];

        if(!empty($errors)) {
            if(!is_array($options['data'])) {
                $options['data'] = (array) $options['data'];
            }
            // Add entity validation errors
            $entity = data_get($options,'data.item');
            if($entity) {
                $entityErrors = $entity->getErrors();
                if(count($entityErrors)) {
                    $entity['errors'] = $entityErrors;
                }
            }
            $options['data']['errors'] = (array) $errors;
        }

        $this->autoRender = false;
        $this->response = $this->response
            ->withType('application/json')
            ->withStringBody(arrayToJson($options['data'],$options['assoc'],false));
    }
    
    
    /**
     * Creates plain text response, rendering specified view, or 
     * default view associated with action.
     * @param array $options - Available options:
     *      data: array|string - If array, data is set as view vars. If string, 
     *          it treats it as already rendered text content. Ignores view and performs no further rendering.
     *      view: Name of template to render,
     *      layout: Name of layout to use. False for no layout. Null for default
     * @return \Cake\Http\Response 
     */
    protected function textResponse($options=[],$errors=null)
    {
        $options += [
            'data' => null,
            'view' => null,
            'layout' => false
        ];

        $this->autoRender = false;
        
        if(!is_string($options['data'])) {
            $builder = $this->viewBuilder();
            $builder->setLayout($options['layout']);
            if (!$builder->getTemplatePath()) {
                $builder->setTemplatePath($this->_viewPath());
            }
                           
            $builder->setClassName($this->viewClass);

            if ($builder->getTemplate() === null && $this->request->getParam('action')) {
                $builder->setTemplate($this->request->getParam('action'));
            }
            if(is_array($options['data'])) {
                $this->set($options['data']);
            }
            if(!empty($errors)) {
                $this->set(compact('errors'));
            }

            $this->View = $this->createView();
            $content = $this->View->render($options['view'], $options['layout']);
        } else {
            $content = $options['data'];
        }
     
        $this->response = $this->response
            ->withType('text/plain')
            ->withStringBody($content);
        
        return $this->response;
    }
    
    /**
     * Normal HTML response
     * @param array $options
     * @param array $errors
     */
    protected function htmlResponse($options=[],$errors=null)
    {
        // Also make errors available to view
        if(!empty($errors)) {
            $options['data']['errors'] = $errors;
        }
        $this->set((array)$options['data']);
        $success = data_get($options,'data.success');
        // Errors
        if (!empty($errors)) {
            foreach((array)$errors as $error) {
                $this->Flash->error($error);
            }
            if(isset($options['data']['errorUrl'])) {
                return $this->redirect($options['data']['errorUrl']);
            }
        }
        // Success
        else {
            if($success) {
                $this->Flash->success($success); 
                if(isset($options['data']['backUrl'])) {
                    return $this->redirect($options['data']['backUrl']);
                }
            }
        }
    }
    
     /**
     * Creates PDF response, rendering specified view, or 
     * default view associated with action.
     * @param array $options - Available options:
     *      data: array|string - If array, data is set as view vars. If string, 
     *          it treats it as already rendered text content. Ignores view and performs no further rendering.
     *      view: Name of template to render,
     *      layout: Name of layout to use. False for no layout. Null for default
     * @param array $errors 
     * @return \Cake\Http\Response 
     */
    protected function pdfResponse($options=[],$errors=null)
    {
        // Default options
        $options += [
            'data' => null,
            'view' => null,
            'layout' => false
        ];

        $this->autoRender = false;

        $pdfConfig = \data_get($options,'pdfConfig');
        
        // HTML based / CakePDF
        if(empty($pdfConfig['pdf'])) {
            // Set view vars
            $builder = $this->viewBuilder();
            $builder->setLayout($options['layout']);
            if (!$builder->getTemplatePath()) {
                $builder->setTemplatePath($this->_viewPath());
            }

            $builder->setClassName($this->viewClass);

            if ($builder->getTemplate() === null && $this->request->getParam('action')) {
                $builder->setTemplate($this->request->getParam('action'));
            }
            // Error check
            if(!empty($errors)) {
                $options['data']['errors'] = $errors;
                //return $this->htmlResponse($options,$errors);
            }
            if(is_array($options['data'])) {
                $this->set($options['data']);
            }
            // PDF configuration
            $config = array_merge(Configure::read('CakePdf') ?: [], data_get($options,'pdfConfig',[]));      
            // Create PDF
            $pdf = new CakePdf($config);
            $pdf->viewVars($this->viewVars);
            $pdf->templatePath($this->_viewPath());
            $pdf->template($this->viewBuilder()->getTemplate(),data_get($options,'layout') ?: 'default');
            $this->response = $this->response
                ->withType('pdf')
                ->withStringBody($pdf->output());
        
            return $this->response;
        }
        // Other
        else {
            $this->response = $this->response->withType('pdf');
            
            if(!$this->is_test) {
                if(class_exists('Cezpdf') && $pdfConfig['pdf'] instanceof \Cezpdf) {
                    $pdfConfig['pdf']->ezStream();
                    exit;
                }
                elseif(class_exists('TCPDF') &&  $pdfConfig['pdf'] instanceof \TCPDF) {
                    $pdfConfig['pdf']->Output(\data_get($pdfConfig,'filename'),\data_get($pdfConfig,'destination','I'));
                    exit;
                }
            }
            return $this->response;
        }
    	
    }
    
     /**
     * Creates Excel spreadsheet response.
     * @param array $options - Available options:
     *      data: array|string - If array, data is set as view vars. If string, 
     *          it treats it as already rendered text content. Ignores view and performs no further rendering.
     *      view: Name of template to render,
     *      layout: Name of layout to use. False for no layout. Null for default
     * @return \Cake\Http\Response 
     */
    protected function xlsResponse($options=[],$errors=null)
    {
        // Default options
        $options += [
            'data' => null,
            'spreadsheet' => null
        ];

        $this->autoRender = false;

        // XLS configuration
        $config = data_get($options,'xlsConfig',[]);
        $spreadsheet = data_get($config,'spreadsheet');
        $title = is_object($spreadsheet) ? $spreadsheet->getProperty('title') : data_get($config,'title','spreadsheet');
        $filename = data_get($config,'filename',$title .'.xls');
    
        $this->response = $this->response
            ->withHeader('Content-Type','application/vnd.ms-excel');
        
        // Output from object - NB: does not work with usual response methods
        if(is_object($spreadsheet) && !$this->is_test) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'. $filename .'"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $spreadsheet->save('php://output');
            exit;
        } 
        // Pass file
        elseif(is_string($spreadsheet) && file_exists($spreadsheet)) {
            $this->response = $this->response
                ->withFile($spreadsheet,['download' => true, 'name' => $filename]);
        }
        // Error
        elseif(!$this->is_test) {
            throw new NotFoundException;
        }
        
        return $this->response;
    }
    
    
    /**
     * Load items for resource, optionally filtered by ids
     * @param string $ids
     */
    public function ajaxLoad()
    {
        // Filters
        $ids = $this->request->getQuery('ids');
        $whitelist = !empty($ids) ? trim_explode(',',$ids) : [];

        if (isset($this->modelClass)) {
            $table = TableRegistry::getTableLocator()->get($this->modelClass);
            $primaryKey = $table->getPrimaryKey();
            $query = $table->find('all');
            if(!empty($whitelist)) {
                $query->where([$primaryKey.' IN' => $whitelist]);
            }
     
            $results = $query->all();
            $data = [];
            foreach($results as $result) {
                $data[$result->$primaryKey] = $result;
            }
            
            $this->jsonResponse(compact('data'));

        } else {
            //@todo throw error if no table.
            throw new \RuntimeException('Controller does not have modelClass!');
        }
      
    }
    
    
    protected function getIdentityUser()
    {
        $identity = $this->Authentication->getIdentity();
        if($identity) {
            $user = $identity->getOriginalData();

            if($user instanceof \ArrayObject) {
                $user = $user->getArrayCopy();
            }
        } else {
            $user = null;
        }
        return $user;
    }
}
