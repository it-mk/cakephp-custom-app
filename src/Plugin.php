<?php
namespace MK\CustomApp;

use Cake\Core\BasePlugin;
use Cake\Core\PluginApplicationInterface;
use Cake\Core\Configure;
use Cake\Http\ServerRequest;
use Cake\Routing\Router;

class Plugin extends BasePlugin
{
    
    public function bootstrap(PluginApplicationInterface $app)
    {
        // Add constants, load configuration defaults.
        // By default will load `config/bootstrap.php` in the plugin.
        parent::bootstrap($app);
        
        Configure::write('Bake.theme', 'MK/CustomApp');

        Router::extensions(['pdf','xls','xlsx']);

        ServerRequest::addDetector('text',['env' => 'HTTP_ACCEPT', 'value' => 'text/plain']);
        ServerRequest::addDetector('pdf', function($request) {
            return ($request->getParam('_ext') === 'pdf' || $request->getQuery('export') === 'pdf');
        });
        ServerRequest::addDetector('xls', function($request) {
            return ($request->getParam('_ext') === 'xls' || $request->getQuery('export') === 'xls');
        });
        ServerRequest::addDetector('xlsx', function($request) {
            return ($request->getParam('_ext') === 'xlsx' || $request->getQuery('export') === 'xlsx');
        });

        // PDF
        Configure::write('CakePdf', [
            'engine' => [
                'className' => 'CakePdf\Pdf\Engine\WkHtmlToPdfEngine',
                'binary' => '/usr/local/bin/wkhtmltopdf'
            ],
            'margin' => [
                'top' => 5,
                'left' => 7,
                'right' => 7,
                'bottom' => 7
            ]
        ]);

        $app->addPlugin(\CakePdf\Plugin::class);
    }

}