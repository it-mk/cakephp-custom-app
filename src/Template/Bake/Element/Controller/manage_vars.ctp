
    /**
     * manageVars method
     * @param array $data - associative array of variables for add/edit methods
     * @return void 
     */
    public function manageVars(&$data)
    {
<%
        $associations = array_merge(
            $this->Bake->aliasExtractor($modelObj, 'BelongsTo'),
            $this->Bake->aliasExtractor($modelObj, 'BelongsToMany')
        );
        foreach($associations as $assoc):
            $association = $modelObj->getAssociation($assoc);
            $otherName = $association->getTarget()->getAlias();
            $otherPlural = $this->_variableName($otherName);
%>
        $data['<%= $otherPlural %>'] = $this-><%= $currentModelName %>-><%= $otherName %>->find('list', ['limit' => 200]);
<%
        endforeach;
%>
    }
