<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
%>

    /**
     * Delete method
     *
     * @param string|null $id <%= $singularHumanName %> id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->autoRender = false;
        $backUrl = $this->request->getQuery('backUrl') ?: ['action' => 'index'];
        $data = [
            'item' => null,
            'backUrl' => $backUrl,
            'errorUrl' => $backUrl
        ];
        $errors = [];
        $this->request->allowMethod(['post', 'delete']);

        try {
            $data['item'] = $this-><%= $currentModelName %>->get($id);

            if(!$this-><%= $currentModelName; %>->delete($data['item'])) {
                $errors[] = __('The <%= strtolower($singularHumanName) %> could not be deleted. Please, try again.');
            } else {
                $data['success'] = __('The <%= strtolower($singularHumanName) %> has been deleted.');
            }
        } catch (\Exception $ex) {
            $errors[] = $ex->getMessage();
        }

        $this->dynamicResponse(compact('data'),$errors);
    }
