    
    /**
     * Manage method (not usually used)
     *
     * @return \Cake\Http\Response|null
     */
    public function manage()
    {
        // Data / errors
        $data = [
            'items' => null
        ];
        $errors = [];

        $this->dynamicResponse(compact('data'),$errors);
    }
    
