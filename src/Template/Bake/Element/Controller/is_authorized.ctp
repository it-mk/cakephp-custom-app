    
    /**
     * isAuthorized method
     *
     * @return boolean
     */
    public function isAuthorized($user)
    {
        $requestAction = $this->request->getParam('action');

        switch($requestAction) {
			case 'index':
			case 'view':
                return true;
                break;
            case 'add': 
			case 'edit':
			case 'manage':
			case 'delete':
            default: 
                return userHasPermission($user,'<%= $currentModelName %>-manage',$this->is_test);
                break;
        }
    }
