<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$compact = ["'" . $singularName . "'"];
%>

    /**
     * Add method
     *
     * @return \Cake\Http\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $data = [
            'item' => $this-><%= $currentModelName %>->newEntity(),
            'backUrl' => $this->request->getQuery('backUrl') ?: ['action' => 'index']
        ];
        $errors = [];

        try {
            if($this->request->is('post')) {
                $data['item'] = $this-><%= $currentModelName %>->patchEntity($data['item'], $this->request->getData());
                if($this-><%= $currentModelName; %>->save($data['item'])) {
                    $data['success'] = __('The <%= strtolower($singularHumanName) %> has been saved.');
                    return $this->dynamicResponse(compact('data'),$errors);
                } else {
                    $errors[] = __('The <%= strtolower($singularHumanName) %> could not be saved. Please, try again.');
                }
            }
        } catch (\Exception $ex) {
            $errors[] = $ex->getMessage();
        }
        /* Template variables */
        if(!$this->request->is('ajax','json')) {
            $this->manageVars($data);
        }

        $this->dynamicResponse(compact('data'),$errors);
    }
