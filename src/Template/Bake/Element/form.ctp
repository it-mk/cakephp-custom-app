<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->getColumnType($field) !== 'binary';
    });

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}
if (!function_exists('showField')) {
    function showField($field) {
        if(substr($field,0,1) === '_') {
            return false;
        }
        if(in_array($field,['archived_at','archived_by','created_by','modified','deleted','deleted_by'])) {
            return false;
        }
        return true;
    }
}

%>
<div class="<%= $pluralVar %> <?= (!empty($is_mobile)) ? 'small-12' : 'large-10 medium-9' ?> columns content">
    <div class="form-heading view-header">
        <div class="row collapse">
            <div class="medium-9 column">
                <h2 class="view-header__title"><?= $this->fetch('legend'); ?>
                </h2>
            </div>
            <div class="medium-3 column view-header__right">
            </div>
        </div>
    </div>

    <div class="form form--single-column">
        <?= $this->Form->create($item) ?>
            <fieldset>
                <div class="row">
<%
        foreach ($fields as $field) {
            if (in_array($field, $primaryKey)) {
                continue;
            }
%>
                    
<%
            if (isset($keyFields[$field]) && showField($field)) {
                $fieldData = $schema->getColumn($field);
                if (!empty($fieldData['null'])) {
%>
                    <div class="medium-4 column">
                        <?= $this->Form->control('<%= $field %>', ['label' => __('<%= Inflector::humanize($field) %>'),
                            'options' => $<%= $keyFields[$field] %>, 'empty' => true]); ?>       
                    </div>
<%
                } else {
%>
                    <div class="medium-4 column">
                        <?= $this->Form->control('<%= $field %>', ['label' => __('<%= Inflector::humanize($field) %>'),
                            'options' => $<%= $keyFields[$field] %>]); ?>
                    </div>
<%
                }
                continue;
            }
            if (showField($field)) {
                $fieldData = $schema->getColumn($field);
                if (in_array($fieldData['type'], ['date', 'datetime', 'time']) && (!empty($fieldData['null']))) {
%>
                    <div class="medium-4 column">
                        <?= $this->Form->control('<%= $field %>', ['label' => __('<%= Inflector::humanize($field) %>'),'empty' => true]); ?>
                    </div>
<%
                } else {
%>
                    <div class="medium-4 column">
                        <?= $this->Form->control('<%= $field %>', ['label' => __('<%= Inflector::humanize($field) %>')]); ?>
                    </div>   
<%
                }
            }
        }
        if (!empty($associations['BelongsToMany'])) {
            foreach ($associations['BelongsToMany'] as $assocName => $assocData) {
%>
            <div class="medium-4 column">
            <?= $this->Form->control('<%= $assocData['property'] %>._ids', ['options' => $<%= $assocData['variable'] %>]); ?>
            </div>
<%
            }
        }
%>
                </div>
                <div class="row form__actions">
                    <div class="small-12 column">
                        <?= $this->Html->link(__('Cancel'),$backUrl,['class' => 'button secondary']); ?>
                        <?= $this->Form->button(__('Submit')) ?>
                    </div>
                </div>
            </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
