<% use Cake\Utility\Inflector; 
%><?php 
$this->extend('/<%= $modelClass %>/manage'); 
$this->assign('legend', __('<%= Inflector::humanize($action) %> <%= $singularHumanName %>') .' #'. $item->id);

?>
