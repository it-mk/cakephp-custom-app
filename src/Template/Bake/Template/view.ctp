<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$associations += ['BelongsTo' => [], 'HasOne' => [], 'HasMany' => [], 'BelongsToMany' => []];
$immediateAssociations = $associations['BelongsTo'];
$associationFields = collection($fields)
    ->map(function($field) use ($immediateAssociations) {
        foreach ($immediateAssociations as $alias => $details) {
            if ($field === $details['foreignKey']) {
                return [$field => $details];
            }
        }
    })
    ->filter()
    ->reduce(function($fields, $value) {
        return $fields + $value;
    }, []);

$groupedFields = collection($fields)
    ->filter(function($field) use ($schema) {
        return $schema->getColumnType($field) !== 'binary';
    })
    ->groupBy(function($field) use ($schema, $associationFields) {
        $type = $schema->getColumnType($field);
        if (isset($associationFields[$field])) {
            return 'string';
        }
        if (in_array($type, ['integer', 'float', 'decimal', 'biginteger'])) {
            return 'number';
        }
        if (in_array($type, ['date', 'time', 'datetime', 'timestamp'])) {
            return 'date';
        }
        return in_array($type, ['text', 'boolean']) ? $type : 'string';
    })
    ->toArray();

$groupedFields += ['number' => [], 'string' => [], 'boolean' => [], 'date' => [], 'text' => []];
$pk = "\$$singularVar->{$primaryKey[0]}";


if (!function_exists('showField')) {
    function showField($field) {
        if(substr($field,0,1) === '_') {
            return false;
        }
        if(in_array($field,['archived_at','archived_by','created_by','modified','deleted','deleted_by'])) {
            return false;
        }
        return true;
    }
}

%><?php 
    $this->assign('title', __('<%= $pluralHumanName %>'));
    $this->assign('legend', __('<%= $singularHumanName %>') .' #'. $item->id);
?>

<div class="<%= $pluralVar %> view <?= (!empty($is_mobile)) ? 'small-12' : 'large-10 medium-9' ?> columns content">
    <div class="view-header">
        <div class="row collapse">
            <div class="medium-9 column">
                <h2 class="view-header__title"><?= $this->fetch('legend'); ?>
                </h2>
            </div>
            <div class="medium-3 column view-header__right header-nav">
                <a href="<?= $this->Url->build($backUrl); ?>" class="button radius secondary" title="Back"><i class="fas fa-angle-left"></i>&nbsp;Back</a>
            </div>
        </div>
    </div>
    <div class="row">
<%
        foreach ($fields as $field) { 
            if(showField($field)) {
%>
        <div class="medium-4 column">
            <?= $this->ViewData->field($item-><%= $field %>,'<%= $field %>',['label' => __('<%= Inflector::humanize($field) %>')]); ?>
        </div>
<%          } 
        }
%>
    </div>
    <?= $this->element('metadata-row',['item' => $item, 'showTitle' => true]); ?>
    <div class="row form__actions">
        <div class="small-12">
            <?= $this->Html->link(__('Back'),$backUrl,['class' => 'button secondary']); ?>
        </div>
    </div>
<%
$relations = $associations['HasMany'] + $associations['BelongsToMany'];
foreach ($relations as $alias => $details):
    $otherSingularVar = Inflector::variable($alias);
    $otherPluralHumanName = Inflector::humanize(Inflector::underscore($details['controller']));
    %>
    <div class="row related">
        <h4><?= __('Related <%= $otherPluralHumanName %>') ?></h4>
        <?php if (!empty($<%= $singularVar %>-><%= $details['property'] %>)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
<% foreach ($details['fields'] as $field): 
    if(!showField($field)) {
        continue;
    }
%>
                <th><?= __('<%= Inflector::humanize($field) %>') ?></th>
<% endforeach; %>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($<%= $singularVar %>-><%= $details['property'] %> as $<%= $otherSingularVar %>): ?>
            <tr>
            <%- foreach ($details['fields'] as $field): %>
                <td><?= h($<%= $otherSingularVar %>-><%= $field %>) ?></td>
            <%- endforeach; %>
            <%- $otherPk = "\${$otherSingularVar}->{$details['primaryKey'][0]}"; %>
                <td class="actions">
                    <?= $this->Html->link(__('live_help'), ['controller' => '<%= $details['controller'] %>', 'action' => 'view', <%= $otherPk %>],['class'=>'material-icons']) ?>
                    <?= $this->Html->link(__('mode_edit'), ['controller' => '<%= $details['controller'] %>', 'action' => 'edit', <%= $otherPk %>],['class'=>'material-icons']) ?>
                    <?= $this->Form->postLink(__('delete'), ['controller' => '<%= $details['controller'] %>', 'action' => 'delete', <%= $otherPk %>], ['class'=>'material-icons','confirm' => __('Are you sure you want to delete # {0}?', <%= $otherPk %>)]) ?>
                </td>
            </tr>
           
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
<% endforeach; %>
</div>
