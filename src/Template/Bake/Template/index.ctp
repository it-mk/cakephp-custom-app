<%
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.1.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Utility\Inflector;

$fields = collection($fields)
    ->filter(function($field) use ($schema) {
        return !in_array($schema->getColumnType($field), ['binary', 'text']);
    });

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
    $fields = $fields->reject(function ($field) {
        return $field === 'lft' || $field === 'rght';
    });
}

if (!empty($indexColumns)) {
    $fields = $fields->take($indexColumns);
}

if (!function_exists('showField')) {
    function showField($field) {
        if(substr($field,0,1) === '_') {
            return false;
        }
        if(in_array($field,['archived_at','archived_by','created_by','modified','deleted','deleted_by'])) {
            return false;
        }
        return true;
    }
}

%><?php 
    $this->assign('title', __('<%= $pluralHumanName %>'));
?>
<div class="<%= $pluralVar %> index <?= (!empty($is_mobile)) ? 'small-12' : 'large-10 medium-9' ?> columns content">
    <h3><?= __('<%= $pluralHumanName %>') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
<% foreach ($fields as $field): 
    if(!showField($field)) {
        continue;
    }
        $fieldLabel = ($field === 'id') ? strtoupper($field) : Inflector::humanize($field);
%>
                <th><?= $this->Paginator->sort('<%= $field %>',__('<%= $fieldLabel %>')) ?></th>
<% endforeach; %>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($items as $item): ?>
            <tr>
<%        foreach ($fields as $field) {
            if(!showField($field)) {
                continue;
            }
            $isKey = false;
            if (!empty($associations['BelongsTo'])) {
                foreach ($associations['BelongsTo'] as $alias => $details) {
                    if ($field === $details['foreignKey']) {
                        $isKey = true;
%>
                <td><?= $item->has('<%= $details['property'] %>') ? $this->Html->link($item-><%= $details['property'] %>-><%= $details['displayField'] %>, ['controller' => '<%= $details['controller'] %>', 'action' => 'view', $item-><%= $details['property'] %>-><%= $details['primaryKey'][0] %>]) : '' ?></td>
<%
                        break;
                    }
                }
            }
            if ($isKey !== true) {
                if (!in_array($schema->getColumnType($field), ['integer', 'biginteger', 'decimal', 'float'])) {
%>
                <td><?= h($item-><%= $field %>) ?></td>
<%
                } else {
%>
                <td><?= $this->Number->format($item-><%= $field %>) ?></td>
<%
                }
            }
        }

        $pk = '$item->' . $primaryKey[0];
%>
                <td class="actions">
                    <?php 
                        echo $this->Html->link(__('live_help'), ['action' => 'view', <%= $pk %>],['class'=>'material-icons']);
                        if(!empty($permissions['manage'])) {
                            echo $this->Html->link(__('mode_edit'), ['action' => 'edit', <%= $pk %>],['class'=>'material-icons']);
                            echo $this->Form->postLink(__('delete'), ['action' => 'delete', <%= $pk %>], ['class'=>'material-icons','confirm' => __('Are you sure you want to delete # {0}?', <%= $pk %>)]); 
                        }
                    ?>
                </td>

            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
